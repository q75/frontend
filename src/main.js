import Vue from 'vue';
import Vuetify from 'vuetify';
import axios from 'axios';
import App from './App';
import Store from './services/store';
import router from "./routes";
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';

Vue.prototype.$store = Store;
const token = localStorage.getItem('token');
if (token) {
    axios.defaults.headers.common['Authorization'] = token;
}

Vue.use(Vuetify, {
    icons: {
        iconfont: 'md'
    }
});

Vue.config.productionTip = false;

new Vue({
    router: router,
    el: '#app',
    components: {App},
    template: '<App/>',
    vuetify: new Vuetify(),
    render: h => h(App)
});
