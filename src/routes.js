import Vue from "vue";
import VueRouter from "vue-router";
import Login from "./components/Login";
import Main from "./components/Main";
import Search from "./components/Search";
import Store from "./services/store";
import Index from "./components/Index";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Index,
        },
        {
            path: '/login',
            component: Login,
        },
        {
            path: '/main',
            component: Main,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/search',
            component: Search,
            meta: {
                requiresAuth: true
            }
        }
    ],
});

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (Store.getters.isLoggedIn) {
            next();
            return;
        }
        next('/login');
    } else {
        next();
    }
});

export default router;
