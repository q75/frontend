const config = {
    http: {
        baseURL: 'http://127.0.0.1:80/api/v1/'
    }
};

export default config;
