export default {
    createErrorMessage: function (err) {
        let errormessage = `<ul>`;
        if (typeof err === 'object') {
            for (const property in err.error) {
                errormessage += `<li>`;
                errormessage += `${err.error[property]}`;
                errormessage += `</li>`;
            }
        } else {
            errormessage += `<li>`;
            errormessage = err;
            errormessage += `</li>`;
        }
        errormessage += `</ul>`;
        return errormessage;
    }
};
