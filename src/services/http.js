import config from "../config/global";
import router from '../routes';
import axios from "axios";

export default {
    getHttp(url, data = null, headers = null) {
        return axios
            .get(config.http.baseURL + url, {
                params: data,
                headers: headers
            })
            .then(response => {
                return response.data;
            })
            .catch(err => {
                if (err.response.status === 401) {
                    router.push('/login');
                }
                if (err.response) {
                    return Promise.reject(err.response.data);
                } else {
                    return Promise.reject(err.message);
                }
            });
    },
    postHttp(url, data = null, headers = null) {
        return axios.post(config.http.baseURL + url, data, {headers: headers})
            .then(function (response) {
                return response.data;
            })
            .catch(function (err) {
                if (err.response.status === 401) {
                    router.push('/login');
                }
                if (err.response) {
                    return Promise.reject(err.response.data);
                } else {
                    return Promise.reject(err.message);
                }
            });
    }
};
